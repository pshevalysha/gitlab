import { initSearchApp } from '~/search';

document.addEventListener('DOMContentLoaded', () => {
  initSearchApp();
});
